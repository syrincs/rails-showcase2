module ApplicationHelper

  def display_base_errors resource
    return '' if (resource.errors.empty?) or (resource.errors[:base].empty?)
    messages = resource.errors[:base].map { |msg| content_tag(:p, msg) }.join
    html = <<-HTML
    <div class="alert alert-error alert-block">
      <button type="button" class="close" data-dismiss="alert">&#215;</button>
      #{messages}
    </div>
    HTML
    html.html_safe
  end
  
  def highlight_admin
#    logger.info params[:controller]
    if
      params[:controller] == "admin/users" ||
      params[:controller] == "admin/roles" ||
      params[:controller] == "admin/categories" ||
      params[:controller] == "admin/subcategories" ||
      params[:controller] == "admin/properties"
    then
      'active'
    end
  end

  def highlight_swaps
    if
      params[:controller] == "admin/swapitems"
    then
      'active'
    end
  end
end
