class Subcategory < ActiveRecord::Base
  include BeautifulScaffoldModule      

  belongs_to :category
  has_many :swapitems, :dependent => :nullify 
  has_many :subcatproperties
  has_many :properties, :through => :subcatproperties

  attr_accessible :category_id, :swapitem_ids, :enabled, :name
  before_save :fulltext_field_processing
  validates_presence_of :name, :on => :create, :message => "can't be blank"
  validates_presence_of :name, :on => :update, :message => "can't be blank"
  validates_presence_of :category_id, :on => :create, :message => "can't be blank"
  validates_presence_of :category_id, :on => :update, :message => "can't be blank"

  def fulltext_field_processing
    # You can preparse with own things here
    generate_fulltext_field([])
  end
  scope :sorting, lambda{ |options|
    attribute = options[:attribute]
    direction = options[:sorting]

    attribute ||= "id"
    direction ||= "DESC"

    order("#{attribute} #{direction}")
  }
    # You can OVERRIDE this method used in model form and search form (in belongs_to relation)
  def caption
    (self["name"] || self["label"] || self["description"] || "##{id}")
  end
end
