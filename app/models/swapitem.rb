class Swapitem < ActiveRecord::Base
  belongs_to :category
  include BeautifulScaffoldModule      

  before_save :fulltext_field_processing
  belongs_to :user
  belongs_to :subcategory
  belongs_to :category
  
  attr_accessible :category_id, :subcategory_id, :coins, :description, :description_fulltext, :description_typetext, :name
  validates_presence_of :name, :on => :create, :message => "can't be blank"
  validates_presence_of :name, :on => :update, :message => "can't be blank"
  validates_presence_of :description, :on => :create, :message => "can't be blank"
  validates_presence_of :description, :on => :update, :message => "can't be blank"
  validates_presence_of :subcategory, :on => :create, :message => "can't be blank"
  validates_presence_of :subcategory, :on => :update, :message => "can't be blank"
  validates_presence_of :category, :on => :create, :message => "can't be blank"
  validates_presence_of :category, :on => :update, :message => "can't be blank"

  def fulltext_field_processing
    # You can preparse with own things here
    generate_fulltext_field(["description"])
  end

  scope :sorting, lambda{ |options|
    attribute = options[:attribute]
    direction = options[:sorting]

    attribute ||= "id"
    direction ||= "DESC"

    order("#{attribute} #{direction}")
  }
    # You can OVERRIDE this method used in model form and search form (in belongs_to relation)
  def caption
    (self["name"] || self["label"] || self["description"] || "##{id}")
  end
end
