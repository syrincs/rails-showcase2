class Category < ActiveRecord::Base
  has_many :swapitems, :dependent => :nullify
  has_many :subcategories, :dependent => :nullify
  include BeautifulScaffoldModule      

  before_save :fulltext_field_processing
  attr_accessible :swapitem_ids, :subcategory_ids, :enabled, :name
  validates_presence_of :name, :on => :create, :message => "can't be blank"
  validates_presence_of :name, :on => :update, :message => "can't be blank"

  def fulltext_field_processing
    # You can preparse with own things here
    generate_fulltext_field([])
  end
  scope :sorting, lambda{ |options|
    attribute = options[:attribute]
    direction = options[:sorting]

    attribute ||= "id"
    direction ||= "DESC"

    order("#{attribute} #{direction}")
  }
    # You can OVERRIDE this method used in model form and search form (in belongs_to relation)
  def caption
    (self["name"] || self["label"] || self["description"] || "##{id}")
  end
  
  def self.active_categories(limit, offset)
    Category.find(:all, :conditions => {:enabled => true}, :order => "name asc", :limit => limit, :offset => offset)
  end
end
