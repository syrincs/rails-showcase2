class Subcatproperty < ActiveRecord::Base
   attr_accessible :subcategory, :property
  belongs_to :subcategory
  belongs_to :property
end
