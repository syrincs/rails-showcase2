class Property < ActiveRecord::Base
  include BeautifulScaffoldModule      

  before_save :fulltext_field_processing
  has_many :subcatproperties
  has_many :subcategories, :through => :subcatproperties

  def fulltext_field_processing
    # You can preparse with own things here
    generate_fulltext_field([])
  end
  scope :sorting, lambda{ |options|
    attribute = options[:attribute]
    direction = options[:sorting]

    attribute ||= "id"
    direction ||= "DESC"

    order("#{attribute} #{direction}")
  }
    # You can OVERRIDE this method used in model form and search form (in belongs_to relation)
  def caption
    (self["name"] || self["label"] || self["description"] || "##{id}")
  end
  attr_accessible :enabled, :name
end
