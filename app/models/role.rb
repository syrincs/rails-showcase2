class Role < ActiveRecord::Base
  include BeautifulScaffoldModule      
  has_and_belongs_to_many :users, :join_table => :users_roles
  belongs_to :resource, :polymorphic => true

  scopify
  before_save :fulltext_field_processing
  attr_accessible :name
  validates_presence_of :name, :on => :create, :error => "can't be blank"
  validates_presence_of :name, :on => :update, :error => "can't be blank"
  
#  validates_presence_of :name, :on => :create, :message => "can't be blank"

  def fulltext_field_processing
    # You can preparse with own things here
    generate_fulltext_field([])
  end
  scope :sorting, lambda{ |options|
    attribute = options[:attribute]
    direction = options[:sorting]

    attribute ||= "id"
    direction ||= "DESC"

    order("#{attribute} #{direction}")
  }
    # You can OVERRIDE this method used in model form and search form (in belongs_to relation)
  def caption
    (self["name"] || self["label"] || self["description"] || "##{id}")
  end
end
