class User < ActiveRecord::Base
  has_many :swapitems, :dependent => :nullify
  include BeautifulScaffoldModule      

  rolify
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :invitable, :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :swapitem_ids, :name, :email, :password, :password_confirmation, :remember_me, :role_ids
  #  scope :with_bio_containing, lambda { |query| where("bio like ?", "%#{query}%").order(:age) }
  #  scope :registered, lambda { |time_ago| { :conditions => ['created_at > ?', time_ago] } }

  before_save :fulltext_field_processing
  validates_presence_of :name, :email, :on => :create, :message => "can't be blank"
  validates_presence_of :name, :email, :on => :update, :message => "can't be blank"
  validates_presence_of :password, :on => :create, :message => "can't be blank"
  validates_presence_of :password, :on => :update, :message => "can't be blank"
  validates_confirmation_of :password

  def fulltext_field_processing
    # You can preparse with own things here
    generate_fulltext_field([])
  end
  scope :sorting, lambda{ |options|
    attribute = options[:attribute]
    direction = options[:sorting]

    attribute ||= "id"
    direction ||= "DESC"

    order("#{attribute} #{direction}")
  }

    # You can OVERRIDE this method used in model form and search form (in belongs_to relation)
  def caption
    (self["name"] || self["label"] || self["description"] || "##{id}")
  end
end
