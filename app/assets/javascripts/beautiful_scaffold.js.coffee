
# PJAX initializer 

# Work on all bootstrap navbar 

#RE-ENABLE PJAX LINKS

# Activate sliders 

# alert(err.message);  //Handle errors here
customcontrols = ->
  $("form[data-validate]").validate()
  $("#dropdownedit .column-menu a").click ->
    name = $(this).attr("data-name") # or can use this.src instead
    catid = $(this).attr("data-catid") # or can use this.src instead
    subcatid = $(this).attr("data-subcatid") # or can use this.src instead
    $("#swapitem_subcategory_name").val name
    $("#swapitem_category_id").val catid
    $("#swapitem_subcategory_id").val subcatid
    $("li.dropdown.open").removeClass "open"

  currentCoins = $("#valueCoins").html()
  $("#mycoins").slider
    orientation: "horizontal"
    range: "max"
    min: 1
    max: 5
    value: currentCoins
    slide: (event, ui) ->
      $("#valueCoins").html ui.value
      $("#swapitem_coins").val ui.value

$ ->
  try
    $("#wrap a:not(.nopjax, .ui-slider-handle)").pjax("#pjax-container",
      timeout: 1000
    ).on "click", ->
      $(this).parent().parent().find(".active").removeClass "active"
      $(this).parent().addClass "active"

    $("#pjax-container").bind("pjax:start", ->
      $(".loader").show()
      $("#pjax-container").fadeOut 150
    ).bind("pjax:end", ->
      $("#pjax-container a:not(.nopjax, .ui-slider-handle)").pjax("#pjax-container").on "click"
      $(".loader").hide()
      $("#pjax-container").fadeIn 150
      customcontrols()
    ).bind "pjax:timeout", ->
      $(".loader").show()
      false

  customcontrols()


# //	This gets the partial via AJAX and just dumps it into the div with id div_id.
# 	$.get("<%= url_for path/to/partial %>",
# 	      function(response) {
# 	        $("#div_id").html(response);
# 	      });

# $(document).ready(function(){
# 
#     /* Richtext editor */
#     $('.select-richtext').click(function(){
#         $('.label-richtext-type[for=' + $(this).attr('id') + ']').trigger('click');
#     });
#     $('.label-richtext-type').live("click", function(){
#         elt = $('#' + $(this).attr('for'));
#         newSet = elt.val();
#         idspleditor = elt.attr('data-spleditor');
#         ideditor = elt.attr('data-editor');
#         $('#' + idspleditor).markItUpRemove();
#         if(!$('#' + idspleditor).hasClass('markItUpEditor')){
#             switch(newSet) {
#                 case 'bbcode':
#                     $('#' + idspleditor).markItUp(myBbcodeSettings);
#                     break;
#                 case 'wiki':
#                     $('#' + idspleditor).markItUp(myWikiSettings);
#                     break;
#                 case 'textile':
#                     $('#' + idspleditor).markItUp(myTextileSettings);
#                     break;
#                 case 'markdown':
#                     $('#' + idspleditor).markItUp(myMarkdownSettings);
#                     break;
#                 case 'html':
#                     $('#' + idspleditor).markItUp(myHtmlSettings);
#                     break;
#             }
#         }
#         $('#' + ideditor).removeClass("bbcode html markdown textile wiki").addClass(newSet);
#         return true;
#     });
# });