# encoding : utf-8
class Admin::SubcategoriesController < BeautifulController

  before_filter :load_subcategory, :only => [:show, :edit, :update, :destroy]

  # Uncomment for check abilities with CanCan
  #authorize_resource

  def index
    session[:fields] ||= {}
    session[:fields][:subcategory] ||= Subcategory.column_names - ['created_at'] - ['updated_at']

    do_select_fields(:subcategory)
    do_sort_and_paginate(:subcategory)
    
    @q = Subcategory.search(
      params[:q]
    )

    @subcategory_scope = @q.result(
      :distinct => true
    ).sorting(
      params[:sorting]
    )
    
    @subcategory_scope_for_scope = @subcategory_scope.dup
    
    unless params[:scope].blank?
      @subcategory_scope = @subcategory_scope.send(params[:scope])
    end
    
    @subcategories = @subcategory_scope.paginate(
      :page => params[:page],
      :per_page => 20
    ).all

    respond_to do |format|
      format.html{
        if request.headers['X-PJAX']
          render :layout => false
        else
          render
        end
      }
      format.json{
        render :json => @subcategory_scope.all 
      }
      format.csv{
        require 'csv'
        csvstr = CSV.generate do |csv|
          csv << Subcategory.attribute_names
          @subcategory_scope.all.each{ |o|
            csv << Subcategory.attribute_names.map{ |a| o[a] }
          }
        end 
        render :text => csvstr
      }
      format.xml{ 
        render :xml => @subcategory_scope.all 
      }             
      format.pdf{
        pdfcontent = PdfReport.new.to_pdf(Subcategory,@subcategory_scope)
        send_data pdfcontent
      }
    end
  end

  def show
    respond_to do |format|
      format.html{
        if request.headers['X-PJAX']
          render :layout => false
        else
          render
        end
      }
      format.json { render :json => @subcategory }
    end
  end

  def new
    @subcategory = Subcategory.new
    @subcategory.enabled = true

    respond_to do |format|
      format.html{
        if request.headers['X-PJAX']
          render :layout => false
        else
          render
        end
      }
      format.json { render :json => @subcategory }
    end
  end

  def edit
    respond_to do |format|
      format.html{
        if request.headers['X-PJAX']
          render :layout => false
        else
          render
        end
      }
      format.json { render :json => @subcategory }
    end
  end

  def create
    @subcategory = Subcategory.create(params[:subcategory])

    respond_to do |format|
      if @subcategory.save
        format.html {
          if params[:mass_inserting] then
            redirect_to admin_subcategories_path(:mass_inserting => true)
          else
#            redirect_to admin_subcategory_path(@subcategory), :notice => t(:create_success, :model => "subcategory")
            redirect_to admin_subcategories_path
          end
        }
        format.json { render :json => @subcategory, :status => :created, :location => @subcategory }
      else
        format.html {
          if params[:mass_inserting] then
            redirect_to admin_subcategories_path(:mass_inserting => true), :error => t(:error, "Error")
          else
            render :action => "new"
          end
        }
        format.json { render :json => @subcategory.errors, :status => :unprocessable_entity }
      end
    end
  end

  def update

    respond_to do |format|
      if @subcategory.update_attributes(params[:subcategory])
        format.html { redirect_to admin_subcategories_path, :notice => t(:update_success, :model => "subcategory") }
        format.json { head :ok }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @subcategory.errors, :status => :unprocessable_entity }
      end
    end
  end

  def destroy
    @subcategory.destroy

    respond_to do |format|
      format.html { redirect_to admin_subcategories_url }
      format.json { head :ok }
    end
  end

  def batch
    attr_or_method, value = params[:actionprocess].split(".")

    @subcategories = []    
    
    Subcategory.transaction do
      if params[:checkallelt] == "all" then
        # Selected with filter and search
        do_sort_and_paginate(:subcategory)

        @subcategories = Subcategory.search(
          params[:q]
        ).result(
          :distinct => true
        )
      else
        # Selected elements
        @subcategories = Subcategory.find(params[:ids].to_a)
      end

      @subcategories.each{ |subcategory|
        if not Subcategory.columns_hash[attr_or_method].nil? and
               Subcategory.columns_hash[attr_or_method].type == :boolean then
         subcategory.update_attribute(attr_or_method, boolean(value))
         subcategory.save
        else
          case attr_or_method
          # Set here your own batch processing
          # subcategory.save
          when "destroy" then
            subcategory.destroy
          end
        end
      }
    end
    
    redirect_to :back
  end

  def treeview

  end

  def treeview_update
    modelclass = Subcategory
    foreignkey = :subcategory_id

    render :nothing => true, :status => (update_treeview(modelclass, foreignkey) ? 200 : 500)
  end
  
  private 
  
  def load_subcategory
    @subcategory = Subcategory.find(params[:id])
  end
end

