# encoding : utf-8
class Admin::SwapitemsController < BeautifulController

  before_filter :load_swapitem, :only => [:show, :edit, :update, :destroy]
  # Uncomment for check abilities with CanCan
  authorize_resource
 
  def index
    session[:fields] ||= {}
    session[:fields][:swapitem] ||= Swapitem.column_names - ['created_at'] - ['updated_at']

    do_select_fields(:swapitem)
    do_sort_and_paginate(:swapitem)
    
    @q = Swapitem.search(
      params[:q]
    )

    @swapitem_scope = @q.result(
      :distinct => true
    ).sorting(
      params[:sorting]
    )
    
    @swapitem_scope_for_scope = @swapitem_scope.dup
    
    unless params[:scope].blank?
      @swapitem_scope = @swapitem_scope.send(params[:scope])
    end
    
    @swapitems = @swapitem_scope.paginate(
      :page => params[:page],
      :per_page => 20
    ).all

    respond_to do |format|
      format.html{
        if request.headers['X-PJAX']
          render :layout => false
        else
          render
        end
      }
      format.json{
        render :json => @swapitem_scope.all 
      }
      format.csv{
        require 'csv'
        csvstr = CSV.generate do |csv|
          csv << Swapitem.attribute_names
          @swapitem_scope.all.each{ |o|
            csv << Swapitem.attribute_names.map{ |a| o[a] }
          }
        end 
        render :text => csvstr
      }
      format.xml{ 
        render :xml => @swapitem_scope.all 
      }             
      format.pdf{
        pdfcontent = PdfReport.new.to_pdf(Swapitem,@swapitem_scope)
        send_data pdfcontent
      }
    end
  end

  def show
    respond_to do |format|
      format.html{
        if request.headers['X-PJAX']
          render :layout => false
        else
          render
        end
      }
      format.json { render :json => @swapitem }
    end
  end

  def new
    catcolumns()
    # @swapitem = Swapitem.new
    # @swapitem.coins = 1
    
    respond_to do |format|
      format.html{
        if request.headers['X-PJAX']
          render :layout => false
        else
          render
        end
      }
      format.json { render :json => @swapitem }
    end
  end

  def edit
    catcolumns()
    
    respond_to do |format|
      format.html{
        if request.headers['X-PJAX']
          render :layout => false
        else
          render
        end
      }
      format.json { render :json => @swapitem }
    end
  end
  
  def create
    @swapitem = Swapitem.new(params[:swapitem])
    @swapitem.user = current_user

    @catid = params[:swapitem][:category_id]
    @subcatid = params[:swapitem][:subcategory_id]
    @catname = Category.find(params[:swapitem][:category_id]).name
    @subcatname = Subcategory.find(params[:swapitem][:subcategory_id]).name 
    
    logger.info ("Ausgabe: " + params[:role][:name].to_s)
    
    respond_to do |format|
      if @swapitem.save
        format.html {
          if params[:mass_inserting] then
            redirect_to admin_swapitems_path(:mass_inserting => true)
          else
#            redirect_to admin_swapitem_path(@swapitem), :notice => t(:create_success, :model => "swapitem")
            redirect_to admin_swapitems_path(:mass_inserting => false), :flash => { :notice => t("Swapitem created!") } 
            
          end
        }
        format.json { render :json => @swapitem, :status => :created, :location => @swapitem }
      else
        format.html {
          if params[:mass_inserting] then
            redirect_to admin_swapitems_path(:mass_inserting => true), :flash => { :error => t("emptyfields")}
          else
            flash.now[:error] = t("emptyfields")
            render "details"
          end
        }
        format.json { render :json => @swapitem.errors, :status => :unprocessable_entity }
      end
    end
  end

  def update

    respond_to do |format|
      if @swapitem.update_attributes(params[:swapitem])
        format.html { redirect_to admin_swapitems_path, :notice => t(:update_success, :model => "swapitem") }
        format.json { head :ok }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @swapitem.errors, :status => :unprocessable_entity }
      end
    end
  end

  def destroy
    @swapitem.destroy

    respond_to do |format|
      format.html { redirect_to admin_swapitems_url }
      format.json { head :ok }
    end
  end

  def batch
    attr_or_method, value = params[:actionprocess].split(".")

    @swapitems = []    
    
    Swapitem.transaction do
      if params[:checkallelt] == "all" then
        # Selected with filter and search
        do_sort_and_paginate(:swapitem)

        @swapitems = Swapitem.search(
          params[:q]
        ).result(
          :distinct => true
        )
      else
        # Selected elements
        @swapitems = Swapitem.find(params[:ids].to_a)
      end

      @swapitems.each{ |swapitem|
        if not Swapitem.columns_hash[attr_or_method].nil? and
               Swapitem.columns_hash[attr_or_method].type == :boolean then
         swapitem.update_attribute(attr_or_method, boolean(value))
         swapitem.save
        else
          case attr_or_method
          # Set here your own batch processing
          # swapitem.save
          when "destroy" then
            swapitem.destroy
          end
        end
      }
    end
    
    redirect_to :back
  end

  def treeview

  end

  def treeview_update
    modelclass = Swapitem
    foreignkey = :swapitem_id

    render :nothing => true, :status => (update_treeview(modelclass, foreignkey) ? 200 : 500)
  end
  
  # def swapitemdetails
  #   @swapitems = Swapitem.find(:all, :select => :name)
  #   
  #     respond_to do |format|
  #         format.html
  #         format.js
  #     end
  # end

   def details
  #    Rails.logger.info "Na das ja mal ne supersache"
      @catid = params[:category_id]
      @subcatid = params[:id]
      @catname = Category.find(params[:category_id]).name
      @subcatname = Subcategory.find(params[:id]).name
      @swapitem = Swapitem.new
      @swapitem.coins = 1
      @tester = Role.new 
      
      respond_to do |format|
        format.html{
          if request.headers['X-PJAX']
            render :layout => false
          else
            render
          end
        }
        format.json { render }
      end

    end
    
  def catcolumns 
    @subcatlength = (Subcategory.count.to_f / 3).ceil       # 3columns for subcategories
    @catlength = (Category.where(enabled: true).count.to_f / 3).ceil        # 3columns for categories
    logger.info "categories_count:" + @catlength.to_s
                                       
    @cat1 = Category.active_categories(@catlength, 0)
    @cat2 = Category.active_categories(@catlength, @catlength)
    @cat3 = Category.active_categories(@catlength, @catlength * 2)
  end
  
  private 
  
  def load_swapitem
    @swapitem = Swapitem.find(params[:id])
  end
end
