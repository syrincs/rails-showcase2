# encoding : utf-8
class Admin::PropertiesController < BeautifulController

  before_filter :load_property, :only => [:show, :edit, :update, :destroy]

  # Uncomment for check abilities with CanCan
  authorize_resource

  def index
    session[:fields] ||= {}
    session[:fields][:property] ||= Property.column_names - ['created_at'] - ['updated_at']
    
    do_select_fields(:property)
    do_sort_and_paginate(:property)
    
    @q = Property.search(
      params[:q]
    )

    @property_scope = @q.result(
      :distinct => true
    ).sorting(
      params[:sorting]
    )
    
    @property_scope_for_scope = @property_scope.dup
    
    unless params[:scope].blank?
      @property_scope = @property_scope.send(params[:scope])
    end
    
    @properties = @property_scope.paginate(
      :page => params[:page],
      :per_page => 20
    ).all

    respond_to do |format|
      format.html{
        if request.headers['X-PJAX']
          render :layout => false
        else
          render
        end
      }
      format.json{
        render :json => @property_scope.all 
      }
      format.csv{
        require 'csv'
        csvstr = CSV.generate do |csv|
          csv << Property.attribute_names
          @property_scope.all.each{ |o|
            csv << Property.attribute_names.map{ |a| o[a] }
          }
        end 
        render :text => csvstr
      }
      format.xml{ 
        render :xml => @property_scope.all 
      }             
      format.pdf{
        pdfcontent = PdfReport.new.to_pdf(Property,@property_scope)
        send_data pdfcontent
      }
    end
  end

  def show
    respond_to do |format|
      format.html{
        if request.headers['X-PJAX']
          render :layout => false
        else
          render
        end
      }
      format.json { render :json => @property }
    end
  end

  def new
    @property = Property.new
    @property.enabled = true

    respond_to do |format|
      format.html{
        if request.headers['X-PJAX']
          render :layout => false
        else
          render
        end
      }
      format.json { render :json => @property }
    end
  end

  def edit
    respond_to do |format|
      format.html{
        if request.headers['X-PJAX']
          render :layout => false
        else
          render
        end
      }
      format.json { render :json => @property }
    end
  end

  def create
    @property = Property.create(params[:property])

    respond_to do |format|
      if @property.save
        format.html {
          if params[:mass_inserting] then
            redirect_to admin_properties_path(:mass_inserting => true)
          else
            redirect_to admin_properties_path, :notice => t(:entry_created)
          end
        }
        format.json { render :json => @property, :status => :created, :location => @property }
      else
        format.html {
          if params[:mass_inserting] then
            redirect_to admin_properties_path(:mass_inserting => true), :error => t(:error, "Error")
          else
            render :action => "new"
          end
        }
        format.json { render :json => @property.errors, :status => :unprocessable_entity }
      end
    end
  end

  def update

    respond_to do |format|
      if @property.update_attributes(params[:property])
        format.html { redirect_to admin_property_path(@property), :notice => t(:update_success, :model => "property") }
        format.json { head :ok }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @property.errors, :status => :unprocessable_entity }
      end
    end
  end

  def destroy
    @property.destroy

    respond_to do |format|
      format.html { redirect_to admin_properties_url }
      format.json { head :ok }
    end
  end

  def batch
    attr_or_method, value = params[:actionprocess].split(".")

    @properties = []    
    
    Property.transaction do
      if params[:checkallelt] == "all" then
        # Selected with filter and search
        do_sort_and_paginate(:property)

        @properties = Property.search(
          params[:q]
        ).result(
          :distinct => true
        )
      else
        # Selected elements
        @properties = Property.find(params[:ids].to_a)
      end

      @properties.each{ |property|
        if not Property.columns_hash[attr_or_method].nil? and
               Property.columns_hash[attr_or_method].type == :boolean then
         property.update_attribute(attr_or_method, boolean(value))
         property.save
        else
          case attr_or_method
          # Set here your own batch processing
          # property.save
          when "destroy" then
            property.destroy
          end
        end
      }
    end
    
    redirect_to :back
  end

  def treeview

  end

  def treeview_update
    modelclass = Property
    foreignkey = :property_id

    render :nothing => true, :status => (update_treeview(modelclass, foreignkey) ? 200 : 500)
  end
  
  private 
  
  def load_property
    @property = Property.find(params[:id])
  end
end

