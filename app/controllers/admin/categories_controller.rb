# encoding : utf-8
class Admin::CategoriesController < BeautifulController

  before_filter :load_category, :only => [:show, :edit, :update, :destroy]

  # Uncomment for check abilities with CanCan
  authorize_resource

  def index
    session[:fields] ||= {}
    session[:fields][:category] ||= Category.column_names - ['created_at'] - ['updated_at']

    do_select_fields(:category)
    do_sort_and_paginate(:category)
    
    @q = Category.search(
      params[:q]
    )

    @category_scope = @q.result(
      :distinct => true
    ).sorting(
      params[:sorting]
    )
    
    @category_scope_for_scope = @category_scope.dup
    
    unless params[:scope].blank?
      @category_scope = @category_scope.send(params[:scope])
    end
    
    @categories = @category_scope.paginate(
      :page => params[:page],
      :per_page => 20
    ).all

    respond_to do |format|
      format.html{
        if request.headers['X-PJAX']
          render :layout => false
        else
          render
        end
      }
      format.json{
        render :json => @category_scope.all 
      }
      format.csv{
        require 'csv'
        csvstr = CSV.generate do |csv|
          csv << Category.attribute_names
          @category_scope.all.each{ |o|
            csv << Category.attribute_names.map{ |a| o[a] }
          }
        end 
        render :text => csvstr
      }
      format.xml{ 
        render :xml => @category_scope.all 
      }             
      format.pdf{
        pdfcontent = PdfReport.new.to_pdf(Category,@category_scope)
        send_data pdfcontent
      }
    end
  end

  def show
    respond_to do |format|
      format.html{
        if request.headers['X-PJAX']
          render :layout => false
        else
          render
        end
      }
      format.json { render :json => @category }
    end
  end

  def new
    @category = Category.new
    @category.enabled = true

    respond_to do |format|
      format.html{
        if request.headers['X-PJAX']
          render :layout => false
        else
          render
        end
      }
      format.json { render :json => @category }
    end
  end

  def edit
    respond_to do |format|
      format.html{
        if request.headers['X-PJAX']
          render :layout => false
        else
          render
        end
      }
      format.json { render :json => @category }
    end
  end

  def create
    @category = Category.create(params[:category])

    respond_to do |format|
      if @category.save
        format.html {
          if params[:mass_inserting] then
            redirect_to admin_categories_path(:mass_inserting => true)
          else
#            redirect_to admin_category_path(@category), :notice => t(:create_success, :model => "category")
            redirect_to admin_categories_path(:mass_inserting => false)
          end
        }
        format.json { render :json => @category, :status => :created, :location => @category }
      else
        format.html {
          if params[:mass_inserting] then
            redirect_to admin_categories_path(:mass_inserting => true), :error => t(:error, "Error")
          else
            render :action => "new"
          end
        }
        format.json { render :json => @category.errors, :status => :unprocessable_entity }
      end
    end
  end

  def update

    respond_to do |format|
      if @category.update_attributes(params[:category])
        format.html { redirect_to admin_categories_path, :notice => t(:update_success, :model => "category") }
        format.json { head :ok }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @category.errors, :status => :unprocessable_entity }
      end
    end
  end

  def destroy
    @category.destroy

    respond_to do |format|
      format.html { redirect_to admin_categories_url }
      format.json { head :ok }
    end
  end

  def batch
    attr_or_method, value = params[:actionprocess].split(".")

    @categories = []    
    
    Category.transaction do
      if params[:checkallelt] == "all" then
        # Selected with filter and search
        do_sort_and_paginate(:category)

        @categories = Category.search(
          params[:q]
        ).result(
          :distinct => true
        )
      else
        # Selected elements
        @categories = Category.find(params[:ids].to_a)
      end

      @categories.each{ |category|
        if not Category.columns_hash[attr_or_method].nil? and
               Category.columns_hash[attr_or_method].type == :boolean then
         category.update_attribute(attr_or_method, boolean(value))
         category.save
        else
          case attr_or_method
          # Set here your own batch processing
          # category.save
          when "destroy" then
            category.destroy
          end
        end
      }
    end
    
    redirect_to :back
  end

  def treeview

  end

  def treeview_update
    modelclass = Category
    foreignkey = :category_id

    render :nothing => true, :status => (update_treeview(modelclass, foreignkey) ? 200 : 500)
  end
  
  private 
  
  def load_category
    @category = Category.find(params[:id])
  end
end

