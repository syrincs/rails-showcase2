# encoding : utf-8
class Admin::RolesController < BeautifulController

  before_filter :load_role, :only => [:show, :edit, :update, :destroy]
  # Uncomment for check abilities with CanCan
  authorize_resource

  def index
    session[:fields] ||= {}
    session[:fields][:role] ||= Role.column_names - ['created_at'] - ['updated_at']

    do_select_fields(:role)
    do_sort_and_paginate(:role)
    
    @q = Role.search(
      params[:q]
    )

    @role_scope = @q.result(
      :distinct => true
    ).sorting(
      params[:sorting]
    )
    
    @role_scope_for_scope = @role_scope.dup
    
    unless params[:scope].blank?
      @role_scope = @role_scope.send(params[:scope])
    end
    
    @roles = @role_scope.paginate(
      :page => params[:page],
      :per_page => 20
    ).all

    respond_to do |format|
      format.html{
        if request.headers['X-PJAX']
          render :layout => false
        else
          render
        end
      }
      format.json{
        render :json => @role_scope.all 
      }
      format.csv{
        require 'csv'
        csvstr = CSV.generate do |csv|
          csv << Role.attribute_names
          @role_scope.all.each{ |o|
            csv << Role.attribute_names.map{ |a| o[a] }
          }
        end 
        render :text => csvstr
      }
      format.xml{ 
        render :xml => @role_scope.all 
      }             
      format.pdf{
        pdfcontent = PdfReport.new.to_pdf(Role,@role_scope)
        send_data pdfcontent
      }
    end
  end

  def show
    respond_to do |format|
      format.html{
        if request.headers['X-PJAX']
          render :layout => false
        else
          render
        end
      }
      format.json { render :json => @role }
    end
  end

  def new
    @role = Role.new

    respond_to do |format|
      format.html{
        if request.headers['X-PJAX']
          render :layout => false
        else
          render
        end
      }
      format.json { render :json => @role }
    end
  end

  def edit
    respond_to do |format|
      format.html{
        if request.headers['X-PJAX']
          render :layout => false
        else
          render
        end
      }
    end
  end

  def create
    @role = Role.create(params[:role])

    respond_to do |format|
      if @role.save
        format.html {
          if params[:mass_inserting] then
            redirect_to admin_roles_path(:mass_inserting => true), :flash => { :notice => "Role created!" }
          else
#            redirect_to admin_role_path(@role), :notice => t(:create_success, :model => "role")
            redirect_to admin_roles_path(:mass_inserting => false), :flash => { :notice => "Role created!" }
          end
        }
        format.json { render :json => @role, :status => :created, :location => @role }
      else
        format.html {
          if params[:mass_inserting] then
            redirect_to admin_roles_path(:mass_inserting => true), :flash => { :error => "Insufficient data!" }
          else
            render :action => "new"
          end
        }
        format.json { render :json => @role.errors, :status => :unprocessable_entity }
      end
    end
  end

  def update

    respond_to do |format|
      if @role.update_attributes(params[:role])
#        format.html { redirect_to admin_role_path(@role), :notice => t(:update_success, :model => "role") }
        format.html { redirect_to admin_roles_path }
        format.json { head :ok }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @role.errors, :status => :unprocessable_entity }
      end
    end
  end

  def destroy
    @role.destroy

    respond_to do |format|
      format.html { redirect_to admin_roles_url }
      format.json { head :ok }
    end
  end

  def batch
    attr_or_method, value = params[:actionprocess].split(".")

    @roles = []    
    
    Role.transaction do
      if params[:checkallelt] == "all" then
        # Selected with filter and search
        do_sort_and_paginate(:role)

        @roles = Role.search(
          params[:q]
        ).result(
          :distinct => true
        )
      else
        # Selected elements
        @roles = Role.find(params[:ids].to_a)
      end

      @roles.each{ |role|
        if not Role.columns_hash[attr_or_method].nil? and
               Role.columns_hash[attr_or_method].type == :boolean then
         role.update_attribute(attr_or_method, boolean(value))
         role.save
        else
          case attr_or_method
          # Set here your own batch processing
          # role.save
          when "destroy" then
            role.destroy
          end
        end
      }
    end
    
    redirect_to :back
  end

  def treeview

  end

  def treeview_update
    modelclass = Role
    foreignkey = :role_id

    render :nothing => true, :status => (update_treeview(modelclass, foreignkey) ? 200 : 500)
  end
  
  private 
  
  def load_role
    @role = Role.find(params[:id])
  end
end

