class ApplicationController < ActionController::Base
  protect_from_forgery

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_path, :alert => exception.message
  end
  
  layout :layout_by_resource

    def layout_by_resource
      if devise_controller? && resource_name == :user && (action_name == 'new' || action_name == 'create') 
        "devise_layout"
      else
        "application"
      end
    end
  
  def after_sign_in_path_for(resource_or_scope)
    admin_dashboard_path
  end
  
  def after_sign_out_path_for(resource_or_scope)
    new_user_session_path
  end               
  
end
